import 'package:flutter/material.dart';
import 'package:shraed_dart/model/post_model.dart';
import 'package:shraed_dart/screens/detail_screen.dart';

class ItemWidget extends StatelessWidget {
  const ItemWidget({Key? key, required this.post}) : super(key: key);

  final PostModel post;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailScreen(
              post: post,
            ),
          ),
        );
      },
      child: ListTile(
        leading: const CircleAvatar(
          backgroundImage: AssetImage(
            'assets/images/user.png',
          ),
          radius: 25,
        ),
        title: Text(post.title),
        subtitle: Column(
          children: [
            Text(
              post.subtitle,
            ),
            const Divider(thickness: 2),
          ],
        ),
      ),
    );
  }
}
