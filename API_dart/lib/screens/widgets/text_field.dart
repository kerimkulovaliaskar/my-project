import 'package:flutter/material.dart';

class TextFieldOfTitle extends StatelessWidget {
  const TextFieldOfTitle({
    super.key,
    required this.title,
    required this.controller,
  });

  final String title;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
          hintText: title,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide.none,
          ),
          fillColor: Colors.grey.shade400,
          filled: true),
    );
  }
}
