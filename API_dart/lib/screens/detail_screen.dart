import 'package:flutter/material.dart';
import 'package:shraed_dart/model/post_model.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({super.key, required this.post});

  final PostModel post;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
          ),
          onPressed: (() {
            Navigator.pop(context);
          }),
        ),
      ),
      body: Center(
          child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            const SizedBox(height: 30),
            const CircleAvatar(
              backgroundImage: AssetImage('assets/images/user.png'),
              radius: 50,
            ),
            const SizedBox(height: 20),
            Text(post.title),
            const SizedBox(height: 20),
            Text(post.subtitle),
          ],
        ),
      )),
    );
  }
}
