import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shraed_dart/model/post_model.dart';
import 'package:shraed_dart/provider/post_provider.dart';
import 'package:shraed_dart/screens/widgets/post_widget.dart';
import 'package:shraed_dart/screens/widgets/text_field.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({
    Key? key,
  }) : super(key: key);

  final TextEditingController? titleController = TextEditingController();
  final TextEditingController? subtitleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final postProvider = context.watch<PostProvider>();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: context.read<PostProvider>().getPosts,
          icon: const Icon(
            Icons.download,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text("Success"),
                    titleTextStyle: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                    actionsOverflowButtonSpacing: 20,
                    actions: [
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text(
                          "Cancel",
                          style: TextStyle(
                            color: Colors.redAccent,
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          if (titleController!.text.isNotEmpty &&
                              subtitleController!.text.isNotEmpty) {
                            postProvider.createPosts(
                              PostModel(
                                id: 101,
                                title: titleController!.text,
                                subtitle: subtitleController!.text,
                              ),
                            );
                            Navigator.of(context).pop();
                          } else {
                            const text = "title or subtitle can't be null";
                            const snackbar = SnackBar(content: Text(text));

                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackbar);
                          }
                        },
                        child: const Text("Done"),
                      ),
                    ],
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFieldOfTitle(
                          title: 'title',
                          controller: titleController!,
                        ),
                        const SizedBox(height: 10),
                        TextFieldOfTitle(
                          title: 'subtitle',
                          controller: subtitleController!,
                        ),
                      ],
                    ),
                  );
                },
              );
            },
            icon: const Icon(
              Icons.add,
            ),
          ),
        ],
      ),
      body: postProvider.isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : postProvider.posts.isNotEmpty
              ? RefreshIndicator(
                  onRefresh: () async {
                    context.read<PostProvider>().getPosts;
                  },
                  child: ListView.separated(
                    padding: const EdgeInsets.all(16),
                    itemCount: postProvider.posts.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (context, index) => Dismissible(
                      key: UniqueKey(),
                      child: ItemWidget(
                        post: postProvider.posts[index],
                      ),
                    ),
                  ),
                )
              : postProvider.errorText.isEmpty
                  ? const Center(
                      child: Text(
                        'Данные еще не загруженны \n Нажмите для загруски!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  : Center(
                      child: Text(
                        postProvider.errorText,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
    );
  }
}
