import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shraed_dart/model/post_model.dart';

class PostRepozitories {
  Future<http.Response> getPosts() async {
    final url = Uri.parse('https://jsonplaceholder.typicode.com/posts');
    return await http.get(url);
  }

  Future<http.Response> createPosts(PostModel postModel) async {
    final url = Uri.parse('https://jsonplaceholder.typicode.com/posts');
    return await http.post(url, body: json.encode(postModel.toJson()));
  }
}
