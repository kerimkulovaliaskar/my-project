import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shraed_dart/provider/post_provider.dart';
import 'package:shraed_dart/repozitories/post_repozitoreis.dart';
import 'package:shraed_dart/screens/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PostProvider(
        repo: PostRepozitories(),
      ),
      child:  MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MyHomePage(),
      ),
    );
  }
}
