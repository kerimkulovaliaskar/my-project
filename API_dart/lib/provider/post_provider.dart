import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shraed_dart/repozitories/post_repozitoreis.dart';

import '../model/post_model.dart';

class PostProvider with ChangeNotifier {
  PostProvider({required this.repo});
  final PostRepozitories repo;
  List<PostModel> posts = [];
  bool isLoading = false;
  String errorText = '';

  Future<void> getPosts() async {
    try {
      isLoading = true;

      notifyListeners();
      final response = await repo.getPosts();
      final data = json.decode(response.body) as List;
      posts = data.map((e) => PostModel.fromjson(e)).toList();
      isLoading = false;
      errorText = '';

      notifyListeners();
    } catch (e) {
      isLoading = false;
      errorText = 'Произошла ошибка!';
      notifyListeners();
    }
  }

  Future<void> createPosts(PostModel postModel) async {
    try {
      isLoading = true;

      notifyListeners();

      await repo.createPosts(postModel);
      await getPosts();

      isLoading = false;
      errorText = '';

      print([
        postModel.id,
        postModel.subtitle,
        postModel.title,
      ]);

      notifyListeners();
    } catch (e) {
      isLoading = false;
      errorText = 'Произошла ошибка!';

      notifyListeners();
    }
  }
}
